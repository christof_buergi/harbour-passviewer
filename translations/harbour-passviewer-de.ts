<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>FirstPage</name>
    <message>
        <source>No passes found</source>
        <translation>Keine Pässe gefunden</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation>Urheberrecht</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Lösche</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <source>pass update successful</source>
        <translation>Passaktualisierung erfolgreich</translation>
    </message>
    <message>
        <source>pass not updateable</source>
        <translation>Pass nicht aktualisierbar</translation>
    </message>
    <message>
        <source>no new version for pass</source>
        <translation>Keine neue Version für diesen Pass</translation>
    </message>
    <message>
        <source>pass update failed</source>
        <translation>Passaktualisierung fehlgeschlagen</translation>
    </message>
    <message>
        <source>Format Error</source>
        <translation>Formatfehler</translation>
    </message>
    <message>
        <source>Couldn&apos;t recognize date/time format</source>
        <translation>Datum/Zeit-Format nicht erkennbar</translation>
    </message>
    <message>
        <source>Unsupported</source>
        <translation>Nicht unterstützt</translation>
    </message>
    <message>
        <source>Please update your system or install calendar</source>
        <translation>Bitte System aktualisieren bzw. Kalender installieren</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Erstelle Kalendereintrag</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <source>Highlight passes close to event time</source>
        <translation>Pässe nahe Zielzeit hervorheben</translation>
    </message>
    <message>
        <source>Time before event</source>
        <translation>Vor Zielzeit</translation>
    </message>
    <message>
        <source>Time after event</source>
        <translation>Nach Zielzeit</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <source>Highlight passes close to destination</source>
        <translation>Pässe nahe Zielort hevorheben</translation>
    </message>
    <message>
        <source>Distance to destination</source>
        <translation>Distanz zum Zielort</translation>
    </message>
    <message>
        <source>Allow passes to override distance</source>
        <translation>Pässe dürfen andere Distanz vorgeben</translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation>Barcode</translation>
    </message>
    <message>
        <source>Show barcode fullscreen when tapped</source>
        <translation>Zeige Barcode Vollbild wenn angetippt</translation>
    </message>
    <message>
        <source>Prefer non-satellite position fixing</source>
        <translation>Bevorzuge satellitenlose Positionsermittlung</translation>
    </message>
    <message>
        <source>Saves battery. &lt;b&gt;Requires&lt;/b&gt; &amp;quot;Faster position fix&amp;quot; switched on in the system settings.</source>
        <translation>Spart Strom. &lt;b&gt;Erfordert&lt;/b&gt; &amp;quot;Schnellere Positionserkennung&amp;quot; in den Systemeinstellungen eingeschaltet.</translation>
    </message>
</context>
<context>
    <name>ShowBack</name>
    <message>
        <source>Simple View</source>
        <translation>einfache Ansicht</translation>
    </message>
</context>
<context>
    <name>ShowPass</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Vollbild Barcode</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>einfache Ansicht</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Erstelle Kalendereintrag</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
</context>
<context>
    <name>ShowSimple</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Vollbild Barcode</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <source>copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
