<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>FirstPage</name>
    <message>
        <source>No passes found</source>
        <translation>Aucun pass trouvé</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <source>pass update successful</source>
        <translation>pass mis à jour avec succès</translation>
    </message>
    <message>
        <source>pass not updateable</source>
        <translation>pass non actualisable</translation>
    </message>
    <message>
        <source>no new version for pass</source>
        <translation>pas de nouvelle version du pass</translation>
    </message>
    <message>
        <source>pass update failed</source>
        <translation>échec de mise à jour du pass</translation>
    </message>
    <message>
        <source>Format Error</source>
        <translation>Format de l&apos;erreur</translation>
    </message>
    <message>
        <source>Couldn&apos;t recognize date/time format</source>
        <translation>Impossible de reconnaître le format date/heure</translation>
    </message>
    <message>
        <source>Unsupported</source>
        <translation>Non pris en charge</translation>
    </message>
    <message>
        <source>Please update your system or install calendar</source>
        <translation>Veuillez mettre à jour votre système ou installer le calendrier</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Créer une entrée de calendrier</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Time</source>
        <translation>Délai</translation>
    </message>
    <message>
        <source>Highlight passes close to event time</source>
        <translation>Mettre en évidence les pass approchant l&apos;horaire</translation>
    </message>
    <message>
        <source>Time before event</source>
        <translation>Délai avant l&apos;événement</translation>
    </message>
    <message>
        <source>Time after event</source>
        <translation>Délai après l&apos;événement</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distance</translation>
    </message>
    <message>
        <source>Highlight passes close to destination</source>
        <translation>Mettre en évidence les pass approchant la destination</translation>
    </message>
    <message>
        <source>Distance to destination</source>
        <translation>Distance jusqu&apos;à destination</translation>
    </message>
    <message>
        <source>Allow passes to override distance</source>
        <translation>Autoriser les pass à outrepasser la distance</translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation>Code-barre</translation>
    </message>
    <message>
        <source>Show barcode fullscreen when tapped</source>
        <translation>Afficher le code-barre en plein écran si appuyé</translation>
    </message>
    <message>
        <source>Prefer non-satellite position fixing</source>
        <translation>Favoriser le positionnement non-satellitaire</translation>
    </message>
    <message>
        <source>Saves battery. &lt;b&gt;Requires&lt;/b&gt; &amp;quot;Faster position fix&amp;quot; switched on in the system settings.</source>
        <translation>Économise la batterie. &lt;b&gt;Nécessite&lt;/b&gt; &amp;quot;Mode d&apos;économie de la batterie&amp;quot; activé dans les paramètres systèmes</translation>
    </message>
</context>
<context>
    <name>ShowBack</name>
    <message>
        <source>Simple View</source>
        <translation>Vue simple</translation>
    </message>
</context>
<context>
    <name>ShowPass</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Code-barre plein écran</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Vuee simple</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Créer un évènement dans le calendrier</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
</context>
<context>
    <name>ShowSimple</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Code-barre plein écran</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <source>copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
