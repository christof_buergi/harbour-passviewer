<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>FirstPage</name>
    <message>
        <source>No passes found</source>
        <translation>Inga pass hittades</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Visa</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Tar bort</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <source>pass update successful</source>
        <translation>passuppdatering slutförd</translation>
    </message>
    <message>
        <source>pass not updateable</source>
        <translation>passet är inte uppdateringsbart</translation>
    </message>
    <message>
        <source>no new version for pass</source>
        <translation>ingen ny passversion</translation>
    </message>
    <message>
        <source>pass update failed</source>
        <translation>passuppdatering misslyckades</translation>
    </message>
    <message>
        <source>Format Error</source>
        <translation>Formatfel</translation>
    </message>
    <message>
        <source>Couldn&apos;t recognize date/time format</source>
        <translation>Känner inte igen datum-/tidsformatet</translation>
    </message>
    <message>
        <source>Unsupported</source>
        <translation>Stöds ej</translation>
    </message>
    <message>
        <source>Please update your system or install calendar</source>
        <translation>Uppdatera ditt system eller installera Kalender</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Skapa kalenderpost</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <source>Highlight passes close to event time</source>
        <translation>Markera pass som är nära händelsetid</translation>
    </message>
    <message>
        <source>Time before event</source>
        <translation>Tid före händelsen</translation>
    </message>
    <message>
        <source>Time after event</source>
        <translation>Tid efter händelsen</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Avstånd</translation>
    </message>
    <message>
        <source>Highlight passes close to destination</source>
        <translation>Markera pass i närheten av platsen</translation>
    </message>
    <message>
        <source>Distance to destination</source>
        <translation>Avstånd till platsen</translation>
    </message>
    <message>
        <source>Allow passes to override distance</source>
        <translation>Låt pass åsidosätta avstånd</translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation>Produktkod</translation>
    </message>
    <message>
        <source>Show barcode fullscreen when tapped</source>
        <translation>Visa produktkoden i helskärm vid tryck</translation>
    </message>
    <message>
        <source>Prefer non-satellite position fixing</source>
        <translation>Föredra icke satellitpositionering</translation>
    </message>
    <message>
        <source>Saves battery. &lt;b&gt;Requires&lt;/b&gt; &amp;quot;Faster position fix&amp;quot; switched on in the system settings.</source>
        <translation>Sparar batteri. &lt;b&gt;Kräver&lt;/b&gt; att &amp;quot;Snabbare positionsbestämning&amp;quot; aktiveras i systeminställningarna.</translation>
    </message>
</context>
<context>
    <name>ShowBack</name>
    <message>
        <source>Simple View</source>
        <translation>Förenklad vy</translation>
    </message>
</context>
<context>
    <name>ShowPass</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Produktkod i helskärm</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Förenklad vy</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Skapa kalenderpost</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
</context>
<context>
    <name>ShowSimple</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Produktkod i helskärm</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <source>copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
