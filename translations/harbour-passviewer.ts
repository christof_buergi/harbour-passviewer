<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>FirstPage</name>
    <message>
        <source>No passes found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass update successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass not updateable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>no new version for pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass update failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Couldn&apos;t recognize date/time format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unsupported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please update your system or install calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Highlight passes close to event time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time before event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time after event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Highlight passes close to destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance to destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allow passes to override distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show barcode fullscreen when tapped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prefer non-satellite position fixing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saves battery. &lt;b&gt;Requires&lt;/b&gt; &amp;quot;Faster position fix&amp;quot; switched on in the system settings.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowBack</name>
    <message>
        <source>Simple View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowPass</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowSimple</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <source>copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
