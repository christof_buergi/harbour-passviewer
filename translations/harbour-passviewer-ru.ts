<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>FirstPage</name>
    <message>
        <source>No passes found</source>
        <translation>Ничего не найдено</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <source>pass update successful</source>
        <translation>карточка обновлена</translation>
    </message>
    <message>
        <source>pass not updateable</source>
        <translation>карточку нельзя обновить</translation>
    </message>
    <message>
        <source>no new version for pass</source>
        <translation>нет обновлений для карточки</translation>
    </message>
    <message>
        <source>pass update failed</source>
        <translation>обновление не удалось</translation>
    </message>
    <message>
        <source>Format Error</source>
        <translation>Ошибка формата</translation>
    </message>
    <message>
        <source>Couldn&apos;t recognize date/time format</source>
        <translation>Не удалось распознать формат даты/времени</translation>
    </message>
    <message>
        <source>Unsupported</source>
        <translation>Не поддерживается</translation>
    </message>
    <message>
        <source>Please update your system or install calendar</source>
        <translation>Пожалуйста, обновите систему или установите приложение календарь</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Добавить в календарь</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <source>Highlight passes close to event time</source>
        <translation>Выделять близкие по времени</translation>
    </message>
    <message>
        <source>Time before event</source>
        <translation>До события</translation>
    </message>
    <message>
        <source>Time after event</source>
        <translation>После события</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <source>Highlight passes close to destination</source>
        <translation>Выделять близкие по расстоянию</translation>
    </message>
    <message>
        <source>Distance to destination</source>
        <translation>Расстояние до места</translation>
    </message>
    <message>
        <source>Allow passes to override distance</source>
        <translation>Разрешить карточкам переопределять расстояние</translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation>Штрих-код</translation>
    </message>
    <message>
        <source>Show barcode fullscreen when tapped</source>
        <translation>Показывать штрих-код на весь экран при касании</translation>
    </message>
    <message>
        <source>Prefer non-satellite position fixing</source>
        <translation>Предпочитать расположение не по спутникам</translation>
    </message>
    <message>
        <source>Saves battery. &lt;b&gt;Requires&lt;/b&gt; &amp;quot;Faster position fix&amp;quot; switched on in the system settings.</source>
        <translation>Экономит заряд. &lt;b&gt;Необходимо&lt;/b&gt; включить опцию &amp;quot;Более быстрое определение расположения&amp;quot; в настройках местонахождения.</translation>
    </message>
</context>
<context>
    <name>ShowBack</name>
    <message>
        <source>Simple View</source>
        <translation>Простой вид</translation>
    </message>
</context>
<context>
    <name>ShowPass</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Штрих-код на весь экран</translation>
    </message>
    <message>
        <source>Simple View</source>
        <translation>Простой вид</translation>
    </message>
    <message>
        <source>Create Calendar Entry</source>
        <translation>Добавить в календарь</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
</context>
<context>
    <name>ShowSimple</name>
    <message>
        <source>Fullscreen Barcode</source>
        <translation>Штрих-код на весь экран</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <source>copied to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
